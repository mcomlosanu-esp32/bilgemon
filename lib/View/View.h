#include <Arduino.h>
#include <Wire.h>

#include <LiquidCrystal_I2C.h> // Library for LCD

class View {
private:
  LiquidCrystal_I2C lcd;

public:
  View(int sda, int clk);
  ~View();
  void start();
  void clearLine(int line);
  void showDateTime(const unsigned int line, const unsigned int idx,
                    DateTime dt);
  void flashLine(const int line, const int times, const char *text);
  void showRecord(const unsigned int idx, const DateTime dt);
  void message(const unsigned int line, const char *message,
               const unsigned int keepMS);
};

/////////////////////////////////////////////////////////////////////////////
void View::start() {
  Wire.begin();

  this->lcd.init();
  this->lcd.backlight();
  this->lcd.setCursor(0, 0);
  this->lcd.clear();
}

View::View(int sda, int clk) : lcd(0x27, 16, 2) {}

View::~View() {}

void View::showDateTime(const unsigned int line, const unsigned int idx,
                        const DateTime dt) {
  char buffer[20];
  sprintf(buffer, "%02u MM/DD hh:mm", idx);
  dt.toString(buffer);
  this->clearLine(line);
  this->lcd.setCursor(0, line);
  this->lcd.print(buffer);
  Serial.println(buffer);
}

void View::flashLine(int line, int times, const char *text) {
  for (int i = 0; i < times; i++) {
    this->lcd.setCursor(0, line);
    this->lcd.print(text);
    delay(500);
    this->clearLine(line);
    delay(500);
  }
}

void View:: clearLine(int line) {
  this->lcd.setCursor(0, line);
  this->lcd.print("                ");
}

void View::showRecord(unsigned int idx, DateTime dt) {
  showDateTime(1, idx, dt);
}

void View::message(unsigned int line, const char *message,
                   unsigned int keepMS) {
  this->clearLine(line);
  this->lcd.setCursor(0, line);
  this->lcd.print(message);
  if (keepMS > 0)
    delay(keepMS);
}