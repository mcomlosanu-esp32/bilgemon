#include "RTClib.h"
#include <Arduino.h>
#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h> // Library for I2C communication

#include <View.h>

#define EEPROM_SIZE 4096
#define TIMESTAMP_SIZE 6
#define TIMESTAMP_EEPROM_ADDRESS 4
#define COUNT_EEPROM_ADDRESS 0
#define BUTTON_DEBOUNCE_TIME 100
#define PUMP_DEBOUNCE_TIME 500

int currRecord;

boolean volatile wait = false;

RTC_DS1307 RTC; // Setup an instance of DS1307 naming it RTC

View view(18, 19);

struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
};

struct Pump {
  const uint8_t PIN;
  uint32_t timesStarted;
  bool started;
};

Button btnUP   = {19, 0, false};
Button btnDOWN = {18, 0, false};
Button btnRESET= {16, 0, false};

Pump pump = {17, 0, false};

void IRAM_ATTR handleUP() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > BUTTON_DEBOUNCE_TIME) {
    btnUP.numberKeyPresses++;
    btnUP.pressed = true;
    wait = false;
  }
  last_interrupt_time = interrupt_time;
}

void IRAM_ATTR handleDOWN() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > BUTTON_DEBOUNCE_TIME) {
    btnDOWN.numberKeyPresses ++;
    btnDOWN.pressed = true;
    wait = false;
  }
  last_interrupt_time = interrupt_time;
}

void IRAM_ATTR handleRESET() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > BUTTON_DEBOUNCE_TIME) {
    btnRESET.numberKeyPresses++;
    btnRESET.pressed = true;
    wait = false;
  }
  last_interrupt_time = interrupt_time;
}

void IRAM_ATTR handlePump() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > PUMP_DEBOUNCE_TIME) {
    pump.started = true;
    pump.timesStarted += 1;
    wait = false;
  }
  last_interrupt_time = interrupt_time;
}

void setupInterrupts() {
  pinMode(btnUP.PIN,   INPUT_PULLUP);
  pinMode(btnDOWN.PIN, INPUT_PULLUP);
  pinMode(btnRESET.PIN, INPUT_PULLUP);
  pinMode(pump.PIN, INPUT_PULLUP);
  attachInterrupt(btnUP.PIN,   handleUP,   RISING);
  attachInterrupt(btnDOWN.PIN, handleDOWN, RISING);
  attachInterrupt(btnRESET.PIN, handleRESET, RISING);
  attachInterrupt(pump.PIN,    handlePump, RISING);
}

void clearEEPROM() {
  view.message(1, "Start clear flash", 1000);
  for (int i = 0; i < EEPROM.length(); i++) {
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
  delay(50);
  EEPROM.put(COUNT_EEPROM_ADDRESS, 0);
  EEPROM.commit();
  delay(50);
  view.message(1, "Done clear flash", 1000);
}

int idxToAddress(int idx) {
  return TIMESTAMP_EEPROM_ADDRESS + TIMESTAMP_SIZE * idx;
}

DateTime getTimeStamp(int fromIdx) {
  DateTime ret;
  EEPROM.get(idxToAddress(fromIdx), ret);
  // Serial.printf("%u -> %d\n", fromIdx, ret.secondstime());
  char buffer[20];
  
  sprintf(buffer, "%02u -> MM/DD hh:mm", fromIdx);
  ret.toString(buffer);

  Serial.println(buffer);

  return ret;
}

void copyTimeStamp(int fromIdx, int toIdx) {
  DateTime dt;
  EEPROM.get(idxToAddress(fromIdx), dt);
  EEPROM.put(idxToAddress(toIdx), dt);
  EEPROM.commit();
}

DateTime storeTimeStamp(int toIdx, DateTime dt) {
  int addr = idxToAddress(toIdx);
  DateTime ret;
  EEPROM.get(addr, ret);
  EEPROM.put(addr, dt);
  EEPROM.commit();
  return ret;
}

void reset() {
  Serial.println("RESET");
  clearEEPROM();
  btnUP.pressed = false;
  btnDOWN.pressed = false;
  btnRESET.pressed = false;
  pump.timesStarted = 0;
  currRecord = 0;
  view.clearLine(1);
}

void dumpEEPROM() {
  for (int i = 0; i < 10; i++) {
    DateTime dt = getTimeStamp(i);
  }
  Serial.printf("\tPump started %d times.\n", pump.timesStarted);
}

bool isOutOfBounds(int idx) {
  Serial.printf("out of bounds check for %u Pump started %u times.\n", idx,
                pump.timesStarted);
  return (idx < 1 || idx > pump.timesStarted);
}

bool hasData() { return pump.timesStarted > 0; }
// ################################################################################
void setup() {
  Serial.begin(115200); // Set serial port speed
  Serial.println("Enter setup");

  Wire.begin(); // Start the I2C
                // clear /EOSC bit to ensure that the clock runs on battery
                // power
  Wire.beginTransmission(0x68); // address DS3231
  Wire.write(0x0E);             // select register
  Wire.write(0b00011100);       // write register bitmap, bit 7 is /EOSC
  Wire.endTransmission();

  delay(100);

  view.start();
  setupInterrupts();

  RTC.begin(); // Init RTC

  //  RTC.adjust(DateTime(__DATE__, __TIME__));  // Time and date is expanded to
  //  date and time on your computer at compiletime Serial.println("Time and
  //  date set");

  
  view.message(0, "Bilge Monitor", 500);
  EEPROM.begin(EEPROM_SIZE);

  // EEPROM.put(COUNT_EEPROM_ADDRESS, 0);
  // EEPROM.commit();

  EEPROM.get(COUNT_EEPROM_ADDRESS, pump.timesStarted);

  delay(50);

  view.showDateTime(0, pump.timesStarted, ( (pump.timesStarted > 0)? getTimeStamp(pump.timesStarted - 1) : RTC.now()) );
  currRecord = ((pump.timesStarted > 0)?1:0);

  if (!hasData()) {
    view.flashLine(1, 3, "No data!");
  }
  else {
    view.showRecord(1, getTimeStamp(1));
  }

  Serial.printf("Setup done. \n\tSaved counter: %u /  EEPROM size "
                "%u\n\tCurrent record: %u\n",
                pump.timesStarted, EEPROM.length(), currRecord);
}




void loop() {
  
  while(wait) {};
  
  if (btnRESET.pressed) {
    Serial.printf("RESET has been pressed.\n");
    reset();
    dumpEEPROM();
    DateTime now = RTC.now();
    view.showDateTime(0, pump.timesStarted, now);
    btnRESET.pressed = false;
  }

  if (btnUP.pressed) {
    Serial.printf("UP has been pressed.\n");
    if( !hasData() ) {
      view.flashLine(1, 3, "No data!");
    } else {
      if (isOutOfBounds(currRecord + 1)) {
        view.flashLine(1, 3, "Reached limit!");
      } else {
        currRecord++;
      }
      view.showRecord(currRecord, getTimeStamp(currRecord-1));
    }
    btnUP.pressed = false;
  }

  if (btnDOWN.pressed) {
    Serial.printf("DOWN has been pressed.\n");
    if (!hasData()) {
      view.flashLine(1, 3, "No data!");
    } else {
      if (isOutOfBounds(currRecord - 1)) {
        view.flashLine(1, 3, "Reached limit!");
      } else {
        currRecord--;
      }
      view.showRecord(currRecord, getTimeStamp(currRecord-1));
    }
    btnDOWN.pressed = false;
  }

  if (pump.started) {
    Serial.printf("Pump started.\n");

    EEPROM.put(COUNT_EEPROM_ADDRESS, pump.timesStarted);
    EEPROM.commit();
    delay(50);

    DateTime now = RTC.now();
    storeTimeStamp(pump.timesStarted - 1, now);
    dumpEEPROM();
    view.showDateTime(0, pump.timesStarted, now);
    pump.started = false;
  }
  
  wait = true;

}

